#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

void loadExposureSeq(String path, vector<Mat>& images, vector<float>& times)
{
    std::setlocale(LC_ALL, "C");
    path = path + std::string("/");
    ifstream list_file((path + "list.txt"));
    string name;
    float val;
    while(list_file >> name >> val) {
        cout << "reading " << name << " " << val << endl;
        Mat img = imread(path + name);
        images.push_back(img);
        times.push_back(1 / val);
    }
}

int main(int, char**argv)
{
    vector<Mat> images;
    vector<float> times;
    loadExposureSeq(argv[1], images, times);

    Mat response;
    Ptr<CalibrateDebevec> calibrate = createCalibrateDebevec();
    calibrate->process(images, response, times);

    Mat hdr;
    Ptr<MergeDebevec> merge_debevec = createMergeDebevec();
    merge_debevec->process(images, hdr, times, response);

    Mat ldr;
    Ptr<TonemapDurand> tonemap = createTonemapDurand(2.2f);
    tonemap->process(hdr, ldr);

    Mat fusion;
    Ptr<MergeMertens> merge_mertens = createMergeMertens();
    merge_mertens->process(images, fusion);

    imwrite("fusion.png", fusion * 255);
    imwrite("ldr.png", ldr * 255);
    imwrite("hdr.hdr", hdr);
    imwrite("hdr.exr", hdr);

    return 0;
}

