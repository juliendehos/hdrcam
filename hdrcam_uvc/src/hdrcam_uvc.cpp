#include <opencv2/opencv.hpp>
#include <libuvc/libuvc.h>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

using namespace std;

// uvc data (webcam)
uvc_context_t * gCxt;
uvc_device_handle_t * gDevh;
uvc_device_t * gDev;
uvc_stream_ctrl_t gCtrl;
uvc_frame_t * gFrame;

// image parameters
int gWidth = 640;
int gHeight = 480;
//vector<uint32_t> gExpos {200, 100, 50, 20};
vector<uint32_t> gExpos {4096, 2048, 1024, 512, 256};
unsigned gNbExpos = gExpos.size();
vector<cv::Mat> gImages(gNbExpos, cv::Mat(gHeight, gWidth, CV_8UC3));

// capture/processing synchronization
int gIdxExpos = 0;
mutex gMutex;

// call uvc function and check return code
void tryUvc(const char * msg, uvc_error_t res) {
    if (res < 0) {
        uvc_perror(res, msg);
        exit(-1);
    }
}

using timePoint_t = std::chrono::time_point<std::chrono::system_clock>;
auto now = std::chrono::system_clock::now;

double duration(const timePoint_t & tp0, const timePoint_t & tp1) {
    std::chrono::duration<double> nbSeconds = tp1 - tp0;
    return nbSeconds.count();
}

// capture callback function
void captureFun(uvc_frame_t * frame, void *) {

    // prevent concurrency (image processing)
    lock_guard<mutex> lock(gMutex);

    // get image from webcam
    tryUvc("any2bgr", uvc_any2bgr(frame, gFrame));

    gIdxExpos = (gIdxExpos + 1) % gNbExpos;

    // change exposure time for next capture
    tryUvc("set_exposure_abs", uvc_set_exposure_abs(gDevh, gExpos[gIdxExpos]));

    // sleep for a while
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
}

int main() {

    // init uvc data (webcam)
    tryUvc("init", uvc_init(&(gCxt), nullptr));
    tryUvc("find_device", uvc_find_device(gCxt, &gDev, 0, 0, nullptr));
    tryUvc("open", uvc_open(gDev, &(gDevh)));
    tryUvc("get_stream_ctrl", uvc_get_stream_ctrl_format_size(gDevh, 
                &(gCtrl), UVC_FRAME_FORMAT_ANY, gWidth, gHeight, 30));
    tryUvc("set_ae_mode", uvc_set_ae_mode(gDevh, 1));
    tryUvc("set_exposure_abs", uvc_set_exposure_abs(gDevh, gExpos[0]));
    gFrame = uvc_allocate_frame(gWidth * gHeight * 3);
    if (not gFrame) {
        cerr << "unable to allocate frame!\n";
        exit(-1);
    }
    tryUvc("start_streaming", uvc_start_streaming(gDevh, 
                &(gCtrl), captureFun, nullptr, 0));

    // init hdr data
    unsigned nbExpos0 = gNbExpos - 1;
    vector<cv::Mat> images0(nbExpos0, cv::Mat(gHeight, gWidth, CV_8UC3));
    vector<float> expos0(nbExpos0);
    for (unsigned i=0; i<nbExpos0; i++) {
        //expos0[i] = float(gExpos[i+1]);
        expos0[i] = 10.0f / float(gExpos[i+1]);
    }

    // main loop
    unsigned iHdr = 0;
    cv::Mat response, hdr, ldr;
    cv::Ptr<cv::CalibrateDebevec> calibrate = cv::createCalibrateDebevec();
    cv::Ptr<cv::MergeDebevec> merge_debevec = cv::createMergeDebevec();
    cv::Ptr<cv::TonemapDurand> tonemap = cv::createTonemapDurand(2.2f);
    while (true) {

        // handle user events
        int key = cv::waitKey(30) % 0x100;
        if (key == 27) 
            break;

        // get and display webcam frame
        // prevent concurrency (webcam capture)
        lock_guard<mutex> lock(gMutex);  

        // get and show frame
        gImages[gIdxExpos] = cv::Mat(gHeight, gWidth, CV_8UC3, gFrame->data);
        if (gIdxExpos != 0) {
            int idx0 = gIdxExpos - 1;
            images0[idx0] = gImages[gIdxExpos].clone();
            cv::imshow(to_string(gExpos[gIdxExpos]), images0[idx0]);
            cout << idx0 << " " 
                << expos0[idx0] << " " 
                << cv::mean(images0[idx0])[1] << endl;
        }

        // compute and show HDR image
        if ((gIdxExpos % gNbExpos) == nbExpos0) {

            // TODO compute alignment

            // calibrate camera
            if (iHdr == 0) {
                cout << "compute HDR ";
                cout.flush();
                timePoint_t t0 = now();
                calibrate->process(images0, response, expos0);
                iHdr = 100;
                timePoint_t t1 = now();
                cout << duration(t0, t1) << "s\n";
            }
            else {
                iHdr--;
            }

            // compute hdr image
            merge_debevec->process(images0, hdr, expos0, response);
            cv::normalize(hdr, hdr, 0, 1, cv::NORM_MINMAX);
            tonemap->process(hdr, ldr);
            cv::imshow("ldr", ldr);
        }
    }

    return 0;
}

